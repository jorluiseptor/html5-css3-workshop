R2-D2, pronounced Artoo-Detoo and often referred to as Artoo, was an R2 series astromech droid manufactured by Industrial Automaton with masculine programming. A smart, spunky droid serving a multitude of masters over its lifetime, R2 has never had a major memory wipe or received new programming resulting in an adventurous and independent attitude. Oftentimes finding himself in pivotal moments in galactic history, his bravery and ingenuity often saved the galaxy time and time again.

Beginning his service in the employ of Queen Amidala of Naboo, the droid would wind up serving prodigal Jedi Knight Anakin Skywalker during the waning years of the Galactic Republic, often accompanied by the protocol droid C-3PO in many adventures throughout the Clone Wars. After his master's turn to the dark side of the Force, he would eventually serve Senator Bail Organa for a time in the Imperial Senate. Nineteen years following the death of the Galactic Republic, he would play a pivotal role in helping the Alliance to Restore the Republic destroy the Empire's dreaded Death Star, carrying technical readouts essential to its destruction. Serving Jedi Purge survivor Luke Skywalker throughout the Galactic Civil War, he would participate in both the Battles of Hoth and Endor, and would witness the destruction of the Empire's dreaded Death Star II a mere four years after the first space station's destruction.

In the subsequent thirty years, Artoo would be set into a self-imposed power mode as newer droid models started to out-compute the aging astromech. Despite this, his celebrated role in the Rebellion protected him from the usual recycling procedure of many old droids, allowing him a peaceful semi-retirement in the Resistance as he pored over several decades of uninterrupted data, causing him to 'dream' of many of his greatest adventures. 

Features
    Rocket booster
    Holoprojector / Recorder
    Periscope
    Fire extinguisher
    Hidden lightsaber compartment with ejector
    Small Saw 
    Electroshock prod (electric shock)
    Data probe
    Utility arm
    Life-form scanner
    Motorized, all-terrain treads 


